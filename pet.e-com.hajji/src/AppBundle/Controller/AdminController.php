<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * @Route("/admin/", name="admin")
     */
    public function adminAction(Request $request)
    {
        $title = "Dashboard";
        
        return $this->render('Admin/dashboard.html.twig',[
            "title" => $title
            ]);
    }
}
 