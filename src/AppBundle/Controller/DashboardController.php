<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Locale;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\FormValidationType; 
use Symfony\Component\Form\Extension\Core\Type\FormBuilder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType; 
use Symfony\Component\Form\Extension\Core\Type\FileType; 
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DashboardController extends Controller
{

    /**
     * @Route("/dashboard/", name="dashboard")
     */
    public function indexAction()
    {
        return $this->render('Dashboard/index.html.twig');
    }


    /**
     * @Route("/dashboard/locale/list/", name="list_locale")
     */
    public function listAction()
    {

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Locale::class);
        $rez = $repo->findAll();


        return $this->render('Dashboard/list.html.twig', ["rez" => $rez]);
    }


    /**
     * @Route("/dashboard/locale/del/{id}", name="del_locale")
     */
    public function delAction($id, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Locale::class);
        $locale = $repo->find($id); 
        $em->remove($locale);
                $em->flush();
                $session = new Session();
                $session->getFlashBag()->add('Delete', 'Locale supprime');
                return $this->redirectToRoute('list_locale'); 

        //return $this->render('Dashboard/list.html.twig', ["rez" => $rez]);
    }


    /**
     * @Route("/dashboard/locale/update/{id}", name="update_locale")
     */
    public function updateAction($id, Request $request)
    {
        //$locale = new Locale();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Locale::class);
        $locale = $repo->find($id); 
        $fb = $this->createFormBuilder($locale)
        ->add('surface', TextType::class)
            ->add('chambres', IntegerType::class)
            ->add('ville', TextType::class)
            ->add('adresse', TextType::class)
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Choisir Type' => null,
                    'Maison' => 'Maison',
                    'Villa' => 'Villa',
                    'Apaprtement' => 'Apaprtement',
                    'Studio' => 'Studio',
                    'Garage' => 'Garage',
                    'Terrain' => 'Terrain',
                    'Bureau' => 'Bureau',
                )))
            ->add('prix', IntegerType::class)
            ->add('services', ChoiceType::class, array(
                'choices' => array(
                    'Climatisation' => 'Climatisation',
                    'Eau Chauffe' => 'Eau Chauffe',
                    'Chauffage Centrale' => 'Chauffage Centrale',
                    'Gaze de ville' => 'Gaze de ville'
                ),
                'choices_as_values' => true, 'multiple' => true, 'expanded' => true
                    ))
            ->add('Modifier', SubmitType::class, array('label' => 'Modifier'));
        $form = $fb->getForm();
            $form -> handleRequest($request);
            if ($form->isSubmitted()) {
                $locale = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($locale);
                $em->flush();
                $session = new Session();
                $session->getFlashBag()->add('Update', 'Locale Mis à jour');
                    return $this->redirectToRoute('list_locale'); 

            }
        return $this->render('Dashboard/update.html.twig', ["rez" => $locale, "form" => $form->createView()]);
    }



    /**
     * @Route("/dashboard/locale/add/", name="add_locale")
     */
    public function addAction(Request $request)
    {

        $locale = new Locale();
        $form = $this->createFormBuilder($locale)
            ->add('surface', TextType::class)
            ->add('chambres', IntegerType::class)
            ->add('ville', TextType::class)
            ->add('adresse', TextType::class)
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Choisir Type' => null,
                    'Maison' => 'Maison',
                    'Villa' => 'Villa',
                    'Apaprtement' => 'Apaprtement',
                    'Studio' => 'Studio',
                    'Garage' => 'Garage',
                    'Terrain' => 'Terrain',
                    'Bureau' => 'Bureau',
                )))
            ->add('prix', IntegerType::class)
            ->add('services', ChoiceType::class, array(
                'choices' => array(
                    'Climatisation' => 'Climatisation',
                    'Eau Chauffe' => 'Eau Chauffe',
                    'Chauffage Centrale' => 'Chauffage Centrale',
                    'Gaze de ville' => 'Gaze de ville'
                ),
                'choices_as_values' => true, 'multiple' => true, 'expanded' => true
                    ))
            ->add('img1', FileType::class, array('label' => 'Photo (png, jpeg)'))
            ->add('img2', FileType::class, array('label' => 'Photo (png, jpeg)'))
            ->add('img3', FileType::class, array('label' => 'Photo (png, jpeg)'))
            ->add('Ajouter', SubmitType::class, array('label' => 'Ajouter Locale'))
            ->getForm();
            $form->handleRequest($request); 
      if ($form->isSubmitted() && $form->isValid()) { 
        $img1 = $locale->getImg1();
        $img2 = $locale->getImg2();
        $img3 = $locale->getImg3(); 
        $fileNameOne  = md5(uniqid()).'.'.$img1->guessExtension();
        $fileNameTwo  = md5(uniqid()).'.'.$img2->guessExtension();
        $fileNameThree  = md5(uniqid()).'.'.$img3->guessExtension(); 
        $img1->move($this->container->getParameter('photos_directory'), $fileNameOne);
        $img2->move($this->container->getParameter('photos_directory'), $fileNameTwo); 
        $img3->move($this->container->getParameter('photos_directory'), $fileNameThree);
        $locale->setImg1($fileNameOne);
        $locale->setImg2($fileNameTwo);
        $locale->setImg3($fileNameThree);
        //on hydrate notre object
        $locale = $form->getData();
        //et on le sauvegarde
        $em = $this->getDoctrine()->getManager();
        $em->persist($locale);
        $em->flush();
                //Session Message
                $session = new Session();
                $session->getFlashBag()->add('Info', 'Locale Ajoute');
                return $this->redirectToRoute('list_locale');   
        }  
        return $this->render('Dashboard/add.html.twig', ["form" => $form->createView()]);      
    }


}
