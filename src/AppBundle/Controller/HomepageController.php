<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Locale;
use AppBundle\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class HomepageController extends Controller
{
 /**
     * @Route("/home/")
     */
    public function indextempAction()
    {
        
      
        //return $this->render('Inscri/index.html.twig');
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Locale::Class);
        $locals = $repo->findAll();
        return $this->render('Homepage/index.html.twig',["locals" => $locals]);

   
    }

    /**
     * @Route("home/page/")
     */
    public function pageAction()
    {
        $em = $this->getDoctrine()->getManager();
        
       
        $repo = $em->getRepository(Locale::Class);
        $locals = $repo->findAll();
       
      //  $catego = $repoca->findAll();
        return $this->render('Homepage/page.html.twig',["locals" => $locals]);
    }
    /**
     * @Route("home/contact/")
     */
    public function contactAction()
    {
        
        
        return $this->render('Homepage/contact.html.twig');
    }
     /**
     * @Route("home/about/")
     */
    public function aboutAction()
    {
        
        
        return $this->render('Homepage/about.html.twig');
    }
    
    /**
     * @Route("home/page/produit/{id}")
     */
    public function produitAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Locale::Class);
        $locals = $repo->find($id);
       
      
        


   


        
        return $this->render('Homepage/produit.html.twig',["local" => $locals]);
    }
}
